import { Component } from "react";
import Header from "../../components/Header/Header.js";
import ProductCard from "../../components/ProductCard/ProductCard.js";
import "./BasketProducts.scss";

const BasketProducts = (props) => {
  return (
    <>
      <ul
        className="products"
        style={{ maxWidth: "1200px", margin: "0 auto " }}
      >
        {props.arrProductsToBuy.map((product) => {
          return (
            <ProductCard
              key={product.vendorCode}
              product={product}
              dataModalId={1}
              addProductsToFavorites={props.addProductsToFavorites}
              deleteCard={true}
              openModal={props.openModal}
              disabledBtn={true}
            />
          );
        })}
      </ul>
    </>
  );
};

export default BasketProducts;

