import { Component } from "react";
import Header from "../../components/Header/Header";
import ProductList from "../../components/ProductList/ProductList";

const Home = (props) => {
  return (
    <>
      <ProductList
        arrProducts={props.arrProducts}
        openModal={props.openModal}
        addProductsToFavorites={props.addProductsToFavorites}
      />
    </>
  );
};

export default Home;
