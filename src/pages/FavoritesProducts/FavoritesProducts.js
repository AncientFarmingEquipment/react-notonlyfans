import { Component } from "react";
import Header from "../../components/Header/Header.js";
import "./FavoritesProducts.scss";
import ProductCard from "../../components/ProductCard/ProductCard.js";

const FavoritesProducts = (props) => {
  return (
    <>
      <ul
        className="products"
        style={{ maxWidth: "1200px", margin: "0 auto " }}
      >
        {props.arrFavoriteProducts.map((product) => {
          return (
            <ProductCard
              key={product.vendorCode}
              product={product}
              openModal={props.openModal}
              addProductsToFavorites={props.addProductsToFavorites}
            />
          );
        })}
      </ul>
    </>
  );
};

export default FavoritesProducts;
