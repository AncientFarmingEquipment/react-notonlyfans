import { Component } from "react";
import "./Header.scss";
import FavoritesHeader from "../FavoritesHeader/FavoritesHeader";
import Basket from "../Basket/Basket";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = (props) => {
  return (
    <>
      <header className="header">
        <Link to="/">
          <div className="header__logo">
            <img src="./img/logo.svg" width="350" height="250" alt="" />
          </div>
        </Link>

        <div>
          <Basket arrProductsToBuy={props.arrProductsToBuy} />
          <FavoritesHeader arrFavoriteProducts={props.arrFavoriteProducts} />
        </div>
      </header>
    </>
  );
};

Header.propTypes = {
  arrFavoriteProducts: PropTypes.array,
  arrProductsToBuy: PropTypes.array,
};

export default Header;

