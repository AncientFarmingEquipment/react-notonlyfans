import { Component } from "react";
import "./FavoritesHeader.scss";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const FavoritesHeader = (props) => {
  return (
    <Link className="header__favorites" to="/favorites">
      <img src="./img/favorite.png" alt="" />
      {props.arrFavoriteProducts !== null && props.arrFavoriteProducts.length}
    </Link>
  );
};

FavoritesHeader.propTypes = {
  arrFavoriteProducts: PropTypes.array,
};

export default FavoritesHeader;
