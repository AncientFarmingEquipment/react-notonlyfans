import "./ProductList.scss";
import ProductCard from "../ProductCard/ProductCard";
import PropTypes from "prop-types";

const ProductList = (props) => {
  const { arrProducts, openModal, addProductsToFavorites } = props;
  return (
    <ul className="products" style={{ maxWidth: "1200px", margin: "0 auto " }}>
      {arrProducts.map((product) => {
        return (
          <ProductCard
            key={product.vendorCode}
            product={product}
            openModal={openModal}
            addProductsToFavorites={addProductsToFavorites}
          />
        );
      })}
    </ul>
  );
};

ProductList.propTypes = {
  arrProducts: PropTypes.array,
  openModal: PropTypes.func,
  addProductsToFavorites: PropTypes.func,
};

export default ProductList;

