import { Component } from "react";
import "./ProductCard.scss";
import Button from "../Button/Button";
import Favorites from "../Favorites/Favorites";
import PropTypes from "prop-types";

const ProductCard = (props) => {
  const { name, price, url, vendorCode, color, handleClick } = props.product;
  return (
    <li className="products__product-item">
      {props.deleteCard && (
        <div
          onClick={() => props.openModal(props.dataModalId, props.product)}
          className="products__product-item-delete"
        >
          +
        </div>
      )}
      <img className="products__product-item-img" src={url}></img>
      <div className="products__product-item-name"> {name}</div>
      <div className="products__product-item-price"> {price}₴</div>
      <div className="products__product-item-color">Колір: {color}</div>
      <div>
        <Favorites
          product={props.product}
          addProductsToFavorites={props.addProductsToFavorites}
          productToFavorites={props.product}
        />
        {!props.disabledBtn && (
          <Button
            text="Add to Card"
            style={{ backgroundColor: "purple" }}
            openModal={props.openModal}
            dataModalId={0}
            product={props.product}
          />
        )}
      </div>
    </li>
  );
};

ProductCard.propTypes = {
  addProductsToFavorites: PropTypes.func,
  openModal: PropTypes.func,
  product: PropTypes.object,
};

export default ProductCard;
